﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TestCode.Models;

namespace TestCode.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly CustomerDB _customerDB;

        public CustomerController(CustomerDB customerDB)
        {
            _customerDB = customerDB;

            if (_customerDB.Customers.Count() == 0)
            {
                _customerDB.Customers.Add(new Customer { CustomerId = 1, FirstName = "Test", Surname = "Man", Dob = DateTime.Now });
                _customerDB.SaveChanges();
            }
        }

        [HttpGet]
        public async Task<ActionResult<List<Customer>>> Get()
        {
            var customers = await _customerDB.Customers.ToAsyncEnumerable().ToList();

            if (customers == null || !customers.Any())
                return NotFound();

            return customers;
        }

        [HttpGet("{name}")]
        public async Task<ActionResult<List<Customer>>> GetByName(string name)
        {
            var customers = await _customerDB.Customers.ToAsyncEnumerable().ToList();

            if (customers == null || !customers.Any())
                return NotFound();

            var newCusList = new List<Customer>();

            foreach(var customer in customers)
            {
                if (customer.FirstName.Contains(name) || customer.Surname.Contains(name))
                    newCusList.Add(customer);
            }

            return newCusList;
        }

        [HttpPost]
        public async Task<ActionResult<Customer>> Post(Customer newCus)
        {
            _customerDB.Customers.Add(newCus);
            await _customerDB.SaveChangesAsync();

            return CreatedAtAction(nameof(Get), new { id = newCus.CustomerId }, newCus);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, Customer curCus)
        {
            if (id != curCus.CustomerId)
            {
                return BadRequest();
            }

            _customerDB.Entry(curCus).State = EntityState.Modified;
            await _customerDB.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTodoItem(int id)
        {
            var customer = await _customerDB.Customers.FindAsync(id);

            if (customer == null)
            {
                return NotFound();
            }

            _customerDB.Customers.Remove(customer);
            await _customerDB.SaveChangesAsync();

            return NoContent();
        }
    }
}