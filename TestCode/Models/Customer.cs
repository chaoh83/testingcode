﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestCode.Models
{
    public class Customer
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }

        public string Surname { get; set; }

        public DateTime Dob { get; set; }
    }
}
