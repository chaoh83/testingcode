﻿const uri = "https://localhost:44334/api/Customer";
let customers = null;

$(document).ready(function () {
    getCustomers();
});

function getCustomers() {
    $.ajax({
        type: "GET",
        url: uri,
        cache: false,
        success: function (data) {
            const tBody = $("#cusList");

            $(tBody).empty();

            $.each(data, function (key, customer) {
                var dateTime = new Date(customer.dob);
                var strDate = dateTime.getDate() + "/" + (dateTime.getMonth() + 1) + "/" + dateTime.getFullYear();

                const tr = $("<tr></tr>")
                    .append($("<td></td>").text(customer.firstName))
                    .append($("<td></td>").text(customer.surname))
                    .append($("<td></td>").text(strDate))
                                .append(
                                    $("<td></td>").append(
                                        $("<button>Edit</button>").on("click", function () {
                                            editItem(customer.customerId);
                                        })
                                    )
                                )
                                .append(
                                    $("<td></td>").append(
                                        $("<button>Delete</button>").on("click", function () {
                                            deleteItem(customer.customerId);
                                        })
                                    )
                                );
                tr.appendTo(tBody);
            });

            customers = data;
        }
    });
}

function addCustomer() {
    const customer = {
        customerId: $("#add-cus-id").val(),
        firstName: $("#add-cus-firstname").val(),
        surname: $("#add-cus-surname").val(),
        dob: $("#add-cus-dob").val()
    };

    $.ajax({
        type: "POST",
        accepts: "application/json",
        url: uri,
        contentType: "application/json",
        data: JSON.stringify(customer),
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Something went wrong!");
        },
        success: function (result) {
            getCustomers();
            $("#add-cus-id").val("");
            $("#add-cus-firstname").val("");
            $("#add-cus-surname").val("");
            $("#add-cus-dob").val("");
        }
    });
}

function deleteItem(id) {
    $.ajax({
        url: uri + "/" + id,
        type: "DELETE",
        success: function (result) {
            getCustomers();
        }
    });
}

function editItem(id) {
    $.each(customers, function (key, customer) {
        var dateTime = new Date(customer.dob);
        var day = ("0" + dateTime.getDate()).slice(-2);
        var month = ("0" + (dateTime.getMonth() + 1)).slice(-2);
        var strDate = dateTime.getFullYear() + "-" + (month) + "-" + (day);

        if (customer.customerId === id) {
            $("#edit-cus-id").val(customer.customerId);
            $("#edit-cus-firstname").val(customer.firstName);
            $("#edit-cus-surname").val(customer.surname);
            $("#edit-cus-dob").val(strDate);
        }
    });
}

$(".my-form").on("submit", function () {
    const customer = {
        customerId: $("#edit-cus-id").val(),
        firstName: $("#edit-cus-firstname").val(),
        surname: $("#edit-cus-surname").val(),
        dob: $("#edit-cus-dob").val()
    };

    $.ajax({
        url: uri + "/" + $("#edit-cus-id").val(),
        type: "PUT",
        accepts: "application/json",
        contentType: "application/json",
        data: JSON.stringify(customer),
        success: function (result) {
            getCustomers();
        }
    });

    return false;
});

$("#search-cus-btn").on("click", function () {
    var key = $("#search-cus").val();

    if (key === null)
        return false;

    $.ajax({
        type: "GET",
        url: uri + "/" + key,
        cache: false,
        success: function (data) {
            const tBody = $("#cusList");

            $(tBody).empty();

            $.each(data, function (key, customer) {
                var dateTime = new Date(customer.dob);
                var strDate = dateTime.getDate() + "/" + (dateTime.getMonth() + 1) + "/" + dateTime.getFullYear();

                const tr = $("<tr></tr>")
                    .append($("<td></td>").text(customer.firstName))
                    .append($("<td></td>").text(customer.surname))
                    .append($("<td></td>").text(strDate))
                    .append(
                        $("<td></td>").append(
                            $("<button>Edit</button>").on("click", function () {
                                editItem(customer.customerId);
                            })
                        )
                    )
                    .append(
                        $("<td></td>").append(
                            $("<button>Delete</button>").on("click", function () {
                                deleteItem(customer.customerId);
                            })
                        )
                    );
                tr.appendTo(tBody);
            });

            customers = data;
        }
    });
})